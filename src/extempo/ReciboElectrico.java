/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extempo;

/**
 *
 * @author elpep
 */
public class ReciboElectrico {
    private int numRecibo;
    private int tipo;
    private String nombre;
    private String fecha;
    private String domicilio;
    private float costo;
    private int kilowat;
    
    public ReciboElectrico() {
        this.numRecibo = 0;
        this.tipo = 0;
        this.nombre = "";
        this.domicilio = "";
        this.fecha = "";
        this.costo = 0.0f;
        this.kilowat = 0;
    }

    public ReciboElectrico(int tipo, String nombre, String fecha, String domicilio, float costo, int kilowat,int numRecibo) {
        this.numRecibo = numRecibo;
        this.tipo = tipo;
        this.nombre = nombre;
        this.fecha = fecha;
        this.domicilio = domicilio;
        this.costo = costo;
        this.kilowat = kilowat;
    }
    
    public ReciboElectrico(ReciboElectrico copia) {
        this.numRecibo = copia.numRecibo;
        this.tipo = copia.tipo;
        this.nombre = copia.nombre;
        this.domicilio = copia.domicilio;
        this.fecha = copia.fecha;
        this.costo = copia.costo;
        this.kilowat = copia.kilowat;
    
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public float getCosto() {
        return costo;
    }

    public void setCosto(float costo) {
        this.costo = costo;
    }

    public int getKilowat() {
        return kilowat;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }
    
    

    public void setKilowat(int kilowat) {
        this.kilowat = kilowat;
    }
  public float calcularSubTotal(){
        float sub = 0.0f;
        switch(this.tipo){
            case 1: 
                this.costo = 2.0f;
                    break;
            case 2:
                this.costo = 3.0f;
                    break;
            case 3:
            this.costo = 5.0f;
                    break;
        }
        sub = this.kilowat * this.costo;
        return sub;
    }
    
    public float calcularImp(){
        float impuesto = 0.0f;
        impuesto = this.calcularSubTotal() * 0.16f;
        return impuesto;
    }
    
    public float calcularTotal(){
        float total = 0.0f;
        total = this.calcularSubTotal() + this.calcularImp();
        return total;
    }
    
}
